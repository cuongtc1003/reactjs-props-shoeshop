import React, { Component } from "react";
import CartShoe from "./CartShoe";
import { data_shoe } from "./data_shoe";
import ListShoe from "./ListShoe";
export default class Ex_Shoe_shop extends Component {
  state = {
    listShoe: data_shoe,
    cart: [],
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (idShoe) => {
    let newCart = this.state.cart.filter((item) => {
      return item.id != idShoe;
    });
    this.setState({
      cart: newCart,
    });
  };
  handleChangeQuantity = (idShoe, luaChon) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == idShoe;
    });
    cloneCart[index].soLuong = cloneCart[index].soLuong + luaChon;
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <h1
          style={{
            boxShadow: " red 0px 3px 10px 3px",
            fontWeight: "bold",
            color: "black",
          }}
          className="mb-5 pt-3 pb-4 text-light"
        >
          Shop Shoes Store
        </h1>
        {this.state.cart.length > 0 && (
          <CartShoe
            handleChange={this.handleChangeQuantity}
            handleDelete={this.handleDelete}
            cart={this.state.cart}
          />
        )}
        <ListShoe handleAdd={this.handleAddToCart} list={this.state.listShoe} />
      </div>
    );
  }
}
